package com.sample.api.dto

import com.sample.app.AppSample
import play.api.libs.json.{Format, Json}

case class AppSampleDto(value: String)

object AppSampleDto {

  def toDto(appSample: AppSample): AppSampleDto = {
    new AppSampleDto(appSample.value)
  }

  implicit val format: Format[AppSampleDto] = Json.format[AppSampleDto]
}