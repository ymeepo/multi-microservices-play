package com.sample.api.controller

import javax.inject._

import com.sample.api.dto.AppSampleDto
import com.sample.app.AppSample
import com.sample.platform.PlatformSample
import play.api.libs.json.Json
import play.api.mvc._

@Singleton
class ApiController @Inject() extends Controller {
  def sample = Action {
    val sample = new AppSample(new PlatformSample("test"))
    Ok(Json.toJson(AppSampleDto.toDto(sample)))
  }
}
