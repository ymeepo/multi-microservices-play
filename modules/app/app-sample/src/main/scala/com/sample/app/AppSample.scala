package com.sample.app

import com.sample.platform.PlatformSample

case class AppSample(platformSample: PlatformSample) {
  def value = platformSample.value
}
