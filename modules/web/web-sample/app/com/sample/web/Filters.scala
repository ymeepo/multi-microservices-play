package com.sample.web

import javax.inject._

import com.sample.web.filters.ExampleFilter
import play.api._
import play.api.http.HttpFilters

/**
 * This class configures com.sample.web.filters that run on every request. This
 * class is queried by Play to get a list of com.sample.web.filters.
 *
 * Play will automatically use com.sample.web.filters from any class called
 * `com.sample.web.Filters` that is placed the root package. You can load com.sample.web.filters
 * from a different class by adding a `play.http.com.sample.web.filters` setting to
 * the `application.conf` configuration file.
 *
 * @param env Basic environment settings for the current application.
 * @param exampleFilter A demonstration filter that adds a header to
 * each response.
 */
@Singleton
class Filters @Inject() (
  env: Environment,
  exampleFilter: ExampleFilter) extends HttpFilters {

  override val filters = {
    // Use the example filter if we're running development mode. If
    // we're running in production or test mode then don't use any
    // com.sample.web.filters at all.
    if (env.mode == Mode.Dev) Seq(exampleFilter) else Seq.empty
  }

}
