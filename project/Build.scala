import play.sbt.PlayScala
import sbt.Keys._
import sbt._

object Build extends sbt.Build {
  val modulesFolder = file("modules")
  val apiFolder = modulesFolder / "api"
  val webFolder = modulesFolder / "web"
  val platformFolder = modulesFolder / "platform"
  val appFolder = modulesFolder / "app"
  val buildsFolder = file("builds")

  val buildVersion = "0.1"
  val scalaCompilerVersion = "2.11.7"

  def buildProject(id: String): Project = {
    Project(id, buildsFolder / id)
  }

  def module(id: String, baseFolder: File): Project = {
    Project(id, baseFolder / id)
      .settings(
        version := buildVersion,
        scalaVersion := scalaCompilerVersion
      )
  }

  def platformModule(id: String): Project = {
    module(id, platformFolder)
  }

  def appModule(id: String): Project = {
    module(id, appFolder)
  }

  def playApp(id: String, baseFolder: File): Project = {
    Project(id, baseFolder).enablePlugins(PlayScala)
      .settings(
        version := buildVersion,
        scalaVersion := scalaCompilerVersion)
  }

  def apiModule(id: String): Project = {
    playApp(id, apiFolder / id)
  }

  def webModule(id: String): Project = {
    playApp(id, webFolder / id)
  }

  // platform modules
  lazy val platformSample = platformModule("platform-sample")

  // app modules
  lazy val appSample = appModule("app-sample").dependsOn(platformSample)

  // api modules
  lazy val apiSample = apiModule("api-sample")
    .dependsOn(appSample)

  // web modules
  lazy val webSample = webModule("web-sample")

  // builds
  lazy val buildPlatform = buildProject("platform-build")
    .aggregate(platformSample)

  // default build
  lazy val root = Project("root", file("."))
      .aggregate(webSample, apiSample)
}


